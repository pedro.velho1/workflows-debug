#!/usr/bin/env python3
# Copyright (c) Ryax Technologies

from ryax_gateway.gateway import RyaxGateway, main

class TestGateway(RyaxGateway):
    """
    Trigger only one empty execution
    """

    async def handler(self):
        await self.send_execution({})


if __name__ == "__main__":
    main(TestGateway)
